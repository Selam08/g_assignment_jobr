import numpy as np
import matplotlib.pyplot as plt
from threading import Thread

def scatter_Location(Id, ax=None, title=None):
    '''Shortcut function in order to scatter binary pixels locations
        Id: boolean matrix with same dimension as image'''
    
    y, x = np.where(Id)
    if ax == None:
        fig, ax = plt.figure()
    ax.imshow(Id)
    ax.scatter(x, y)
    if title:
        ax.set_title(title)
        
        
#=======================================================================================
# Denoising processing based on median filter
#=======================================================================================

def Denoise_byMedian(img_Band, Corrupted_Id, verbose=False, set2zero=False):
    '''Achieve denoising processing on a unique band
    img_Band : 2d array of image
    Corrupted_Id: 2d, boolean index of corruped pixels locations
    set2zero : boolean for conversion of corrupted nan pixel in 0'''
    
    # Import
    from skimage.util import pad
    
    # Initialize variables
    Padded_img = pad(img_Band,1,mode='symmetric')
    Corrupted_Id = pad(Corrupted_Id,1,mode='symmetric')
    Denoised_img_Band = np.full(Padded_img.shape, np.nan, Padded_img.dtype)
    Denoised_img_Band[~Corrupted_Id] = Padded_img[~Corrupted_Id]

    # Lauch image processing
    for i in range(1, Padded_img.shape[0]-1):
        for j in range(1, Padded_img.shape[1]-1):

            # Check if current pixel is corrupted
            if Corrupted_Id[i, j] == True:
                Si, Sj = slice(i-1, i+2), slice(j-1, j+2)
                Denoised_img_Band[i, j] = np.median(
                    Padded_img[Si, Sj][~Corrupted_Id[Si, Sj]])
        
        # Print information
        if verbose:
            print('\r  Achieved processing:  {0:d} %'.format(int(100*(i-2)/img_Band.shape[0])), end=' ')
        if set2zero:
            Denoised_img_Band[np.isnan(Denoised_img_Band)] = 0
        
    return Denoised_img_Band[1:-1, 1:-1]

def Denoise_byAdaptativeMedian(img_Band, Corrupted_Id, verbose=False, set2zero=False):
    '''Achieve denoising processing on a unique band with adaptative size of moving window
    img_Band : 2d array of image
    Corrupted_Id: 2d, boolean index of corruped pixels locations
    set2zero : boolean for conversion of corrupted nan pixel in 0'''
    
    # Import
    from skimage.util import pad
    
    # Initialize variables
    Padded_img = pad(img_Band,3,mode='symmetric')
    Corrupted_Id = pad(Corrupted_Id,3,mode='symmetric')
    Denoised_img_Band = np.full(Padded_img.shape, np.nan, Padded_img.dtype)
    Denoised_img_Band[~Corrupted_Id] = Padded_img[~Corrupted_Id]

    # Lauch image processing
    for i in range(3, Padded_img.shape[0]-3):
        for j in range(3, Padded_img.shape[1]-3):

            # Check if current pixel is corrupted
            if Corrupted_Id[i, j] == True:
                for r in range(1,4): 
                    Si, Sj = slice(i-r, i+r+1), slice(j-r, j+r+1)
                    Denoised_img_Band[i, j] = np.median(
                        Padded_img[Si, Sj][~Corrupted_Id[Si, Sj]])
                    if not np.isnan(Denoised_img_Band[i, j]):
                        break
                    
        # Print information
        if verbose:
            print('\r  Achieved processing:  {0:d} %'.format(int(100*(i-2)/img_Band.shape[0])), end=' ')
        if set2zero:
            Denoised_img_Band[np.isnan(Denoised_img_Band)] = 0
        
    return Denoised_img_Band[3:-3, 3:-3]


def Denoise_RGB_byMedian(img):
    '''Achieve denoising processing on a RGB image
    img : 3d array of color image'''

    # Global index of corrupted pixel location
    Corrupted_Id = img == 0
    Denoised_img = np.full(img.shape, np.nan, img.dtype)

    # Lauch processing on bands (one by one)
    for i in range(3):
        print('\n Processing band {0:d}'.format(i+1), end='\n')
        Denoised_img[:, :, i] = Denoise_byMedian(
            img[:, :, i], Corrupted_Id[:, :, i], verbose=True)
        
    # Look for resulting corrupted pixels
    print('\n Process ended, number of resulting corrupted pixel : {0:d}'.format(np.sum(np.isnan(Denoised_img))))

    # Translate corrupted pixel from nan to 0
    Denoised_img[np.isnan(Denoised_img)] = 0
    return Denoised_img


def Full_denoisingPipeline(img, method, max_iter=10):
    '''Achieve iterative denoising processing until complete restoration
    img: array of image or band to restore
    method: processing function'''
    
    # Initialize variables
    Count_Corrupted_Pixel = np.sum(img==0)
    OldCount_Corrupted_Pixel = Count_Corrupted_Pixel +1
    Count = 1
    
    # Launch iterative processing
    while Count_Corrupted_Pixel != OldCount_Corrupted_Pixel and Count_Corrupted_Pixel != 0:
        print('\nIteration {0:d}:'.format(Count), end='')
        img = method(img)
        Count = Count+1
        
        # safety
        if Count>max_iter:
            return img
            break
        
        # Update variables
        OldCount_Corrupted_Pixel = Count_Corrupted_Pixel
        Count_Corrupted_Pixel = np.sum(img==0)
        
    return img

#=======================================================================================
# Adaptation of denoising processing to parralel processing
#=======================================================================================

class FullMulti_denoisingPipeline(Thread):
    '''Threading class for achieving parallel denoising process of bands separately
    
    Designed for multithreading'''
    
    def __init__(self, img, name, adaptativeMode=False, max_iter=10):
        '''Initialization method :
        img : 2d array of band or gray image
        name : string of bandname or image name
        adaptativeMode : bolean allowing to use adaptative size of moving window
        max_iter : int, maximum of iteration'''
        
        Thread.__init__(self)
        self.img = img
        self.name = name
        self.max_iter = max_iter
        self.adatptative = adaptativeMode
        self.denoised = None

    def run(self):
        
        # Print info
        print('starting process on band {0:s}'.format(self.name))
        
        # Initialize variables
        Count_Corrupted_Pixel = np.sum(self.img==0)
        OldCount_Corrupted_Pixel = Count_Corrupted_Pixel +1
        Count = 1

        # Launch iterative processing
        while Count_Corrupted_Pixel != OldCount_Corrupted_Pixel and Count_Corrupted_Pixel != 0:
            print('\nIteration {0:d} on band: {1:s}'.format(Count, self.name), end='')
            if self.adatptative:
                self.img = Denoise_byAdaptativeMedian(self.img, self.img==0, set2zero=True)
            else: 
                self.img = Denoise_byMedian(self.img, self.img==0, set2zero=True)
            Count = Count+1
            
            # safety
            if Count>self.max_iter:
                self.denoised = self.img
                break

            # Update variables
            OldCount_Corrupted_Pixel = Count_Corrupted_Pixel
            Count_Corrupted_Pixel = np.sum(self.img==0)
        self.denoised = self.img

def RGB_Multiprocess_Pipeline(img, adaptative=False):
    '''Achieve iterative denoising processing until complete restoration
    img: array of image to restore
    adaptative: bolean allowing to use adaptative size of moving window
    
    design for parallel processing'''


    # define band name
    BandName = ['R', 'G', 'B']

    # Setup a list of processes that we want to run
    processes = [FullMulti_denoisingPipeline(img[:,:,b], BandName[b], adaptativeMode=adaptative, max_iter=10) for b in range(3)]

    # Run processes
    for p in processes:
        p.start()

    # Exit the completed processes
    for p in processes:
        p.join()

    # Get process results from the output queue
    Out = {p.name: p.denoised for p in processes}

    
    return np.stack((Out['R'], Out['G'], Out['B']), axis=2)

#=======================================================================================
#=======================================================================================