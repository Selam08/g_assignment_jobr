def Get_Normlizes(data):
    """
    Normalizes M to be within range [0, 1].
    M: array of data to normalize
    """
    
    min_Val = np.min(data)
    max_Val = np.max(data)

    data_red = data - min_Val;

    if max_Val == min_Val:
        return np.zeros(data.shape);
    else:
        return data_red / (max_Val-min_Val)