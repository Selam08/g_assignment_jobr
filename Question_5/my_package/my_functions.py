import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn import mixture
import matplotlib as mpl
import itertools
from scipy import linalg
from sklearn.preprocessing import StandardScaler
from scipy.interpolate import interp1d

def Print_Dickey_test(Data, Autolag='AIC'):
    from statsmodels.tsa.stattools import adfuller
    
    '''Perform and print result of Dickey test applied on Data.
    Data : timeserie'''
    
    # Launch test
    results = adfuller(Data, autolag=Autolag)
    
    # Print Results
    ToPrint = pd.Series(results[0:4], index=['Test Statistic','P-value','Used Lags','Number of Observations Used'])
    for key, value in results[4].items():
        ToPrint['MacKinnon Critical Value (%s)'%key] = value
    print (ToPrint)

def kpss_test(timeseries):
    from statsmodels.tsa.stattools import kpss

    kpsstest = kpss(timeseries, regression='c')
    results = pd.Series(kpsstest[0:3], index=['Test Statistic','p-value','Lags Used'])
    for key,value in kpsstest[3].items():
        results['Critical Value (%s)'%key] = value
    print (results)
    
def Get_Breakdate_bySignal(df, DoPlot=False, ax=None):
    '''Detect date corresponding of break point in timeserie df
    df : pandas datafram'''
    
    Avg_df = df.rolling(3).mean(center=True)
    Hpass_Sig = np.full((len(Avg_df)), np.nan, float)
    Hpass_Sig[1:-1] = Avg_df[:-2].values - Avg_df[2:].values
    
    Breakdate_Pos = np.nanargmax(Hpass_Sig)
    
    if DoPlot == True:
        if not ax == None:
            pd.DataFrame(data= Hpass_Sig, index=df.index, columns=[df.name]).plot(ax=ax,use_index=False)
            ax.plot(np.arange(df.shape[0]), np.ones(df.index.shape)*np.nanmax(Hpass_Sig), 'k--', label='Maximum delta')
            ax.legend(loc='upper right')
        else:
            fig = plt.figure()
            pd.DataFrame(data= Hpass_Sig, index=df.index, columns=[df.name]).plot(use_index=False)
            plt.plot(np.arange(df.shape[0]), np.ones(df.index.shape)*np.nanmax(Hpass_Sig),'k--', label='Maximum delta')
            plt.legend(loc='upper right')
     
    return Breakdate_Pos, df.index[Breakdate_Pos]




def plot_results(X, Y, means, covariances, DropPos, ax, title):
    '''Taken from sciki-learn'''
    
    color_iter = ['c','gold', 'cornflowerblue', 'gold',
                          'darkorange']
    
    for i, (mean, covar, color) in enumerate(zip(
            means, covariances, color_iter)):
        v, w = linalg.eigh(covar)
        v = 2. * np.sqrt(2.) * np.sqrt(v)
        u = w[0] / linalg.norm(w[0])
        # as the DP will not use every component it has access to
        # unless it needs it, we shouldn't plot the redundant
        # components.
        if not np.any(Y == i):
            continue
        if i ==0:
            ax.scatter(X[Y == i, 0], X[Y == i, 1], color='k', s=0.8, label='Smoothed time-series')
        ax.scatter(X[Y == i, 0], X[Y == i, 1], color='k', s=0.8)
        Cent_m = (means[np.argsort(means[:,0])[1],0], means[np.argsort(means[:,0])[1],1])
        

        # Plot an ellipse to show the Gaussian component
        angle = np.arctan(u[1] / u[0])
        angle = 180. * angle / np.pi  # convert to degrees
        ell = mpl.patches.Ellipse(mean, v[0], v[1], 180. + angle, color=color)
        ell.set_clip_box(ax.bbox)
        ell.set_alpha(0.5)
        ax.add_artist(ell)
        
    ax.scatter(Cent_m[0], Cent_m[1], s=20, color='r')
    ax.plot([Cent_m[0], DropPos[0]], [Cent_m[1], Cent_m[1]], 'r--', label='Time to drop date')
    ax.legend()

    plt.title(title)
    plt.xticks(())
    plt.yticks(())

def Get_GMM_Scores(df, DropDate, n_cp=1):
    ''''''
    # format
    N_cp = [n_cp, 1, n_cp]
    df = df.rolling(3).mean(center=True)
    y = mpl.dates.date2num(df.index.to_pydatetime())
    X = np.stack((y, df.values), axis=1)
    
    # standardize
    scaler = StandardScaler()
    X[1:-1,:] = scaler.fit_transform(X[1:-1,:])    
    
    # train gaussians
    Id = [np.arange(1, DropDate[0]-1), 
          np.arange(DropDate[0]-1, DropDate[0]+2), 
          np.arange(DropDate[0]+2, X.shape[0]-1)]
    gmm = [mixture.GaussianMixture(n_components=N_cp[i],
                                   covariance_type='full',
                                   max_iter=100).fit(X[Id[i]]) for i in range(len(Id))]
    
    # record means
    means = [gmm[i].means_[0] for i in range(len(Id))]
    means = np.stack(means, axis=0)
    
    # record covariances
    covariances = [gmm[i].covariances_[0] for i in range(len(Id))]
    covariances = np.stack(covariances, axis=0)

    # create index
    Index = np.ones((X.shape[0],))
    ad = [0, n_cp, 1+n_cp]
    scores = []
    
    # Compute scores
    for i in range(len(Id)):
        Index[Id[i]] = gmm[0].predict(X[Id[i]])+ad[i]
    scores.append([gmm[i].score(X[Id[i]]) for i in range(len(Id))])

    return X, means, covariances, Index, scores

def Get_FreeGMM(df, DropDate, n_cp=3):
    '''Train mixture of gaussians to detect drop in time serie'''
    
    # format
    df = df.rolling(5).mean(center=True)
    y = mpl.dates.date2num(df.index.to_pydatetime())
    X = np.stack((y, df.values), axis=1)
    
    # standardize
    scaler = StandardScaler()
    X[2:-2,:] = scaler.fit_transform(X[2:-2,:])
    
    # Store Means to Initilize
    Means_init = X[2,:], X[DropDate[0],:], X[-3,:]
    
    # Generate additional point
    f2 = interp1d(X[2:-2,0], X[2:-2,1], kind='linear')
    xnew = np.linspace(X[2,0], X[-3,0], num=300, endpoint=True)
    Xnew = np.stack((xnew, f2(xnew)), axis=1)
    Xint = np.full((Xnew.shape[0]+4, Xnew.shape[1]), np.nan, dtype=float)
    Xint[:2,0], Xint[2:-2,:], Xint[-2:,0] = X[:2,0], Xnew, X[-2:,0]
    X = Xint
    del xnew, Xnew, Xint 
    
    # train gaussians
    gmm = mixture.GaussianMixture(n_components=n_cp,
                                   covariance_type='full',
                                   max_iter=100, means_init=Means_init).fit(X[2:-2])
    
    # record means
    means = gmm.means_
    
    # record covariances
    covariances = gmm.covariances_

    # create index
    Index = np.ones((X.shape[0],))
    Index[2:-2] = gmm.predict(X[2:-2])
    ad = [0, n_cp, 1+n_cp]
    scores = []

    return X, means, covariances, Index, gmm